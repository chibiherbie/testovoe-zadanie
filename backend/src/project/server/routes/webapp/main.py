from project.server import app, templates
from fastapi import Request
from project.db.models import User


@app.get('/api/telegram/webapp/{user_id}')
async def index(request: Request, user_id: int):
    user = await User.get_or_none(id=user_id)
    return templates.TemplateResponse("index.html",
                                      {
                                          "request": request,
                                          "name": user.entered_name,
                                          "age": user.entered_age,
                                          "count": await User.filter(is_registered=True).count(),
                                      })
