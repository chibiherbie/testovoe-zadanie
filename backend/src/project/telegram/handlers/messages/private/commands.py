from aiogram import types
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext

from project.telegram import dp
from project.telegram.states import Register, Base
from project.telegram.utils.answer import answer
from project.telegram.utils.handler import handler
from project.telegram.utils.webapp import send_webapp


@dp.message(Command('start'))
@handler
async def start(message: types.Message, state: FSMContext):
    user_state = await state.get_state()

    match user_state:
        case Base.idle:
            await send_webapp(message, message.from_user.id)
        case _:
            await message.answer("Привет! Для продолжения работы бота, нужно пройти мини регистрацию\n"
                                 f"Напиши своё имя")
            await state.set_state(Register.get_name)
