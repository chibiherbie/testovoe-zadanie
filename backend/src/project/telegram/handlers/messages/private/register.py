from typing import Dict

from aiogram import types

from project.telegram import dp
from project.telegram.utils.answer import answer
from project.telegram.utils.handler import handler
from aiogram.fsm.context import FSMContext
from aiogram import F
from project.db.models import User
from project.telegram.states import Register, Base
from project.telegram.utils.webapp import send_webapp


@dp.message(Register.get_name)
@handler
async def handle_name(message: types.Message, state: FSMContext):
    await state.update_data(name=message.text)

    await message.answer(text="Приятно познакомится. Теперь напишите Ваш возраст")
    await state.set_state(Register.get_age)


@dp.message(Register.get_age, F.text.isdigit())
@handler
async def handle_age(message: types.Message, user: User, state: FSMContext):
    await state.update_data(age=message.text)

    await save_user_info(user, await state.get_data())

    await message.answer('Спасибо за регистрацию!')
    await send_webapp(message, user.id)

    await state.set_state(Base.idle)


async def save_user_info(user: User, user_data: Dict):
    user.entered_name = user_data['name']
    user.entered_age = user_data['age']
    user.is_registered = True
    await user.save()


@dp.message(Register.get_age)
@handler
async def expect_age(message: types.Message):
    await message.answer(text="Пожалуй, Вы где-то ошиблись."
                              " Давайте попробуем ещё раз, нужно написать возраст в виде числа")
