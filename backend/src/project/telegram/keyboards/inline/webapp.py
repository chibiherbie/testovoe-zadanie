from aiogram import types
from aiogram.types import InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder

from project.settings import settings


def webapp_builder(user_id) -> InlineKeyboardMarkup:

    url = f'https://{settings.DOMAIN}/api/telegram/webapp/{user_id}'

    builder = InlineKeyboardBuilder()
    builder.button(
        text='Приложение',
        web_app=types.WebAppInfo(url=url)
    )
    return builder.as_markup()
