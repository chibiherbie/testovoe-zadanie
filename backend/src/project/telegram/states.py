from aiogram.fsm.state import StatesGroup, State


class Base(StatesGroup):
    idle = State()


class Register(StatesGroup):
    get_name = State()
    get_age = State()
