from aiogram import types

from project.telegram.keyboards.inline.webapp import webapp_builder
from project.db.models import User


async def send_webapp(message: types.Message, user_id: int):
    await message.answer(
        text=f"Открыть приложение",
        reply_markup=webapp_builder(user_id)
    )
